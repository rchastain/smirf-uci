//================================================
// PROJECT: SmirfEngine.dll (C) R. Scharnagl
//================================================

#ifndef SmirfEngineH
#define SmirfEngineH


// --- to be defined when included in its *.cpp ---
#ifdef  SmirfDLL
#define IMP_EXP dllexport
#else
#define IMP_EXP dllimport
#endif


#include "../Schach/Mov.h"
#include "../Schach/Hsh.h"

#ifdef __BCPLUSPLUS__
#define ENGINE_NAME "Smirf"
#else
#define ENGINE_NAME "SMIRF"
#endif

// the TMCI engine interface function: DoCmd()

extern "C" __declspec(IMP_EXP) const char * __cdecl DoCmd(
  char MainCmd, char SubCmd, int nrIn = 0, const char *pIn = 0L);

/*
  TMCI protocol (Third Millennium Chess Interface) [ incomplete draft ]

  The protocol is intended to keep the GUI realy silly. The GUI
  gets all its knowlegde on chess moves from the engines. This
  makes those GUIs much easier to be extended to new variants,
  and also allows an easier writing of such GUIs. 
 

  COMMANDS (please discuss planned extensions with R. Scharnagl)

  How to perform any command:

  (DoCmd args)      (DoCmd answer)

   '*' '*' ** ** => char string "<answer>" rsp. "ok" at success
                    (and stop any engine run task, if necessary)
                 => otherwise 0L, if impossible or after errors
                    (query last error reason by: DoCmd('I','R'))

  (ALREADY DONE)

  Query:
   '?' '*' -- -- => list of all sub commands to any main command '*'
   '?' '?' -- -- => list of all main commands

  Info:
   'I' 'A' -- -- => name of the author
   'I' 'E' -- -- => name of the engine
   'I' 'V' -- -- => version of the engine
   'I' 'R' -- -- => reason (of the last error situation)

  FEN-Position:
   'F' 'S' -- -- => produce the FEN char string matching the
                    current situation
   'F' '4' nr -- => produce the FEN char string to a numbered
                    Chess480 starting position (<nr> 1...960)
                    (or else 0L, when not FRC 8x8 aware)
   'F' '8' nr -- => produce the FEN char string to a numbered
                    chess960 8x8 starting position (<nr> 1...960)
                    (or else 0L, when not FRC 8x8 aware)
            0 -- => produce the FEN char string to
                    the classical 8x8 chess starting array
                    (or else 0L, when not standard 8x8 aware)
   'F' 'C' nr vv => produce the FEN char string to a numbered (not
                    selected) CRC 10x8 starting position (<nr> 1...48000)
                    (or else 0L, when not CRC 10x8 aware if vv != 0L)
            0 -- => produce the FEN char string to
                    the Capablanca 10x8 chess starting array
                    (or else 0L, when not Capablanca 10x8 aware)
           -1 -- => produce the FEN char string to
                    the MBC Chess 10x8 chess starting array
                    (or else 0L, when not MBC Chess 10x8 aware)
           -2 -- => produce the FEN char string to
                    the Janus Chess 10x8 chess starting array
                    (or else 0L, when not Janus Chess 10x8 aware)
           -3 -- => produce the FEN char string to
                    the Bird 10x8 chess starting array
                    (or else 0L, when not Bird 10x8 aware)
           -4 -- => produce the FEN char string (Pos. 25495) to
                    the Carrera 10x8 chess starting array
                    (or else 0L, when not Carrera 10x8 aware)
           -5 -- => produce the FEN char string (Pos. 25933) to
                    the GothicChess 10x8 chess starting array
                    (or else 0L, when not GothicChess 10x8 aware)
           -6 -- => produce the FEN char string (Pos. 27016) to
                    the Optimized Chess 10x8 chess starting array
                    (or else 0L, when not Optimized Chess 10x8 aware)
  Move:
   'M' 'A' nr -- => show the currently prepared move <nr> in
                    algebraic form (letter, digit, letter, digit, extra),
                    source before target, where extra is the lower promotion
                    piece code (English) or zero. When a King's castling
                    move does less than two elementary steps, the involved
                    Rook's coordinate will be used as target coordinate.
                    (or else 0L, if move <nr> does not exist)
                    This codes are sent back to the engine in case that
                    a move would be selected.
   'M' 'T' nr -- => show the currently prepared move <nr> as long text
                    (e.g. "e4xf5+", "O-O-O" or "Bc1-f4" using English)
                    (or else 0L, when move <nr> does not exist)
   'M' 'S' nr -- => show the currently prepared move <nr> as short text
                    (e.g. "ef5+", "O-O-O" or "Bf4" using English)
                    (or else 0L, when move <nr> does not exist)
   'M' 'V' nr -- => show the evaluation to currently prepared move <nr>
                    (or else 0L, when move <nr> does not exist)
   'M' 'L' nr -- => show the search level of currently prepared move <nr>
                    (or else 0L, when move <nr> does not exist)

  Enter:
   'E' 'F' -- fs => enter a FEN char string <fs> as position
                    (and prepare a list of actual moves)
   'E' 'M' -- mv => enter a move <mv>
                    (as been encoded before by DoCmd('M', 'A', nr))
                    (and prepare FEN and a list of actual moves)
   'E' '-' -- -- => take back last move
                    (and prepare FEN and a list of actual moves)

  Licensing: (to be set (once) before calculating or pondering)
   'L' 'N' -- nm => input the user name <nm>
                    (will answer a string truncated to < 255 chars)
   'L' 'K' -- ky => input the engine-key <ky>
                    (will answer "ok" when key matches or engine is not
                     protected at all, or 0L if necessary key was wrong)
                    (the procedure should not work after some bad trials)

  Timing: (to be set befor pondering or computing)
   'T' 'W' tm -- => Time Left White, in MSec, totale Restzeit
   'T' 'B' tm -- => Time Left Black, in MSec, totale Restzeit
   'T' '#' =1 -- => Time is for one move (=1) only or all (=0) moves

  Computing:
   'C' 'P' -- -- => let the engine start its Ponder-thinking
   'C' '+' -- -- => let the engine start its thinking
   'C' '-' -- -- => let the engine terminate its thinking
   'C' 'B' -- -- => busy? answer with "ok" or 0L
   'C' 'I' 00 -- => Info: answer with (last) actual move
   'C' 'I' 01 -- => Info: answer with (last) actual value
   'C' 'I' 02 -- => Info: answer with (last) actual PV
   'C' 'A' -- -- => request answer-move in algebraic notation
   'C' 'H' -1 -- => query hash size as a char string
   'C' 'H' mb -- => set new hash size in MB and query result

  (TODO)

   'T' 'F' tm -- => Fischerzeit pro Zug (falls geändert)

  Computing:

*/
//-------------------------------------------------------