
uses
  SysUtils, ChessTypes, XFen;

procedure Test(const AFen: string);
var
  data: TChessPositionData;
begin
  data := EncodeChessPositionData(AFen);
  WriteLn('<- ', AFen);
  WriteLn('-> ', DecodeChessPositionData(data));
  with data.castling do
    WriteLn('xk=', xk, ' xrWH=', xrWH, ' xrWA=', xrWA, ' xrBH=', xrBH, ' xrBA=', xrBA);
end;

begin
  Test('nrnkbrqb/pppppppp/8/8/8/8/PPPPPPPP/NRNKBRQB w FBfb - 0 1');
  Test('nrnkbrqb/pppppppp/8/8/8/8/PPPPPPPP/NRNKBRQB w KQkq - 0 1');
  Test('nrnkbrqb/pppppppp/8/8/8/8/PPPPPPPP/RRNKB1QB w Bfb - 0 1');
end.
