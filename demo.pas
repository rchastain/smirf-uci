
{$ASSERTIONS ON}

uses
  SysUtils, Smirf;

var
  LSmirf: TSmirf;

function CountLegalMoves(const ADepth: integer): integer;
// Jouer et reprendre un coup
var
  i: integer;
  r: string;
begin
  result := 0;
  i := 0;
  r := LSmirf.DoCmd('M', 'A', i); // Obtenir l'énième coup
  while r <> '' do
  begin
    if ADepth > 1 then
    begin
      Assert(LSmirf.DoCmd('E', 'M', 0, r) <> ''); // Jouer le coup
      Inc(result, CountLegalMoves(Pred(ADepth)));
      Assert(LSmirf.DoCmd('E', '-') <> ''); // Reprendre le coup
    end else
      Inc(result);
    Inc(i);
    r := LSmirf.DoCmd('M', 'A', i);
  end;
end;

const
  CUser = 'Donationware Version - donate !';
  CUserOk = CUser;
  CKey = '342f6f5e 026b3179';
  CKeyOk = 'Smirf';
  
var
  r: string;
  i: integer;
  
begin
  LSmirf := TSmirf.Create;
  try
    if LSmirf.Load then
    begin
      // Informations sur le moteur
      WriteLn(LSmirf.DoCmd('I', 'E')); // Nom
      WriteLn(LSmirf.DoCmd('I', 'V')); // Version
      WriteLn(LSmirf.DoCmd('I', 'A')); // Auteur
      
      // Licence
      Assert(LSmirf.DoCmd('L', 'N', 0, CUser) = CUserOk); // Nom
      Assert(LSmirf.DoCmd('L', 'K', 0, CKey) = CKeyOk); // Clé
      
      // Liste des commandes
      r := LSmirf.DoCmd('?', '?');
      for i := 1 to Length(r) do
        if r[i] <> '?' then
          WriteLn(r[i], ' ', LSmirf.DoCmd('?', r[i]));
      
      // Entrer une position
      WriteLn(LSmirf.DoCmd('E', 'F', 0, 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1'));
      
      // Obtenir la position courante
      WriteLn(LSmirf.DoCmd('F', 'S'));
      
      // Régler le temps
      WriteLn(LSmirf.DoCmd('T', 'W', 1000));
      WriteLn(LSmirf.DoCmd('T', 'B', 1000));
      WriteLn(LSmirf.DoCmd('T', '#', 0));
      
      // Obtenir la liste des coups
      i := 0;
      r := LSmirf.DoCmd('M', 'A', i);
      while r <> '' do
      begin
        WriteLn(r);
        Inc(i);
        r := LSmirf.DoCmd('M', 'A', i);
      end;
      
      // Jouer et reprendre un coup
      WriteLn(CountLegalMoves(2));
    end;
  finally
    LSmirf.Free;
  end;
end.
