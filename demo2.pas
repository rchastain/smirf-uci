
uses
  SysUtils, Smirf;
  
const
  CUser = 'Donationware Version - donate !';
  CUserOk = CUser;
  CKey = '342f6f5e 026b3179';
  CKeyOk = 'Smirf';
  COk = 'ok';
  CDefaultVersion = 180;
  
var
  LSmirf: TSmirf;
  LMove: string;
  LMoveCount: integer;
  
begin
  LSmirf := TSmirf.Create;
  try
    if LSmirf.Load then
    begin
      WriteLn(LSmirf.DoCmd('I', 'E')); // Nom du moteur
      WriteLn(LSmirf.DoCmd('I', 'V')); // Version
      WriteLn(LSmirf.DoCmd('I', 'A')); // Auteur
      WriteLn(LSmirf.DoCmd('L', 'N', 0, CUser) = CUserOk); // Nom
      WriteLn(LSmirf.DoCmd('L', 'K', 0, CKey) = CKeyOk);   // Clé
      
      WriteLn(LSmirf.DoCmd('E', 'F', 0, 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1') = COk);
      WriteLn(LSmirf.DoCmd('E', 'M', 0, 'e2e4') = COk);
      WriteLn(LSmirf.DoCmd('F', 'S') = 'rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq - 0 1');
      
      LMoveCount := 0;
      LMove := LSmirf.DoCmd('M', 'A', LMoveCount);
      while LMove <> '' do
      begin
        Inc(LMoveCount);
        WriteLn(LMoveCount, '. ', LMove);
        LMove := LSmirf.DoCmd('M', 'A', LMoveCount);
      end;
    end;
  finally
    LSmirf.Free;
  end;
end.
