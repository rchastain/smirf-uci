
unit Utils;

interface

function BeginsWith(const ASubStr, AStr: string): boolean;
function WordsNumber(const AStr: string): integer;
function WordPresent(const AWord, AStr: string): boolean;
function GetWord(const AIndex: integer; const AStr: string): string;
function GetFen(const AStr: string): string;
function IsChessMove(const AStr: string): boolean;
function IsGoCmd(const AStr: string; out MTime: integer): boolean; overload; // go movetime 500
function IsGoCmd(const AStr: string; out WTime, BTime: integer): boolean; overload; // go wtime 600000 btime 600000
function IsGoCmd(const AStr: string; out WTime, BTime, MTG: integer): boolean; overload; // go wtime 59559 btime 56064 movestogo 38
function IsGoCmd(const AStr: string; out WTime, BTime, WInc, BInc: integer): boolean; overload; // go wtime 60000 btime 60000 winc 1000 binc 1000
function IsPerftCmd(const AStr: string; out ADepth: integer): boolean;
function IsSetOptionCmd(const AStr: string; out AName, AValue: string): boolean;

implementation

uses
  SysUtils, StrUtils, RegExpr;

type
  TExpr = (eG1, eG2, eG3, eG4, eWT, eBT, eWI, eBI, eSO);

const
  CExpr: array[TExpr] of string = (
    'go movetime (\d+)',
    'go [wb]time \d+ [wb]time \d+',
    'go [wb]time \d+ [wb]time \d+ movestogo (\d+)',
    'go [wb]time \d+ [wb]time \d+ [wb]inc \d+ [wb]inc \d+',
    'wtime (\d+)',
    'btime (\d+)',
    'winc (\d+)',
    'binc (\d+)',
    'setoption name (.+) value (.+)'
  );
  
var
  LExpr: array[TExpr] of TRegExpr;

function BeginsWith(const ASubStr, AStr: string): boolean;
begin
  result := Pos(ASubStr, AStr) = 1;
end;

function WordsNumber(const AStr: string): integer;
begin
  result := WordCount(AStr, [' ']);
end;

function WordPresent(const AWord, AStr: string): boolean;
begin
  result := IsWordPresent(AWord, AStr, [' ']);
end;

function GetWord(const AIndex: integer; const AStr: string): string;
begin
  result := ExtractWord(AIndex, AStr, [' ']);
end;

function GetFen(const AStr: string): string;
// position fen qbbnnrkr/pppppppp/8/8/8/8/PPPPPPPP/QBBNNRKR w HFhf - 0 1 moves e2e4
begin
  result := Format('%s %s %s %s %s %s', [
    GetWord(3, AStr),
    GetWord(4, AStr),
    GetWord(5, AStr),
    GetWord(6, AStr),
    GetWord(7, AStr),
    GetWord(8, AStr)
  ]);
end;

function IsChessMove(const AStr: string): boolean;
begin
  result := (Length(AStr) >= 4)
    and (AStr[1] in ['a'..'h'])
    and (AStr[2] in ['1'..'8'])
    and (AStr[3] in ['a'..'h'])
    and (AStr[4] in ['1'..'8']) or (AStr = 'O-O-O') or (AStr = 'O-O');
end;

function IsGoCmd(const AStr: string; out MTime: integer): boolean;
// go movetime 500
begin
  MTime := 0;
  
  result := LExpr[eG1].Exec(AStr) and TryStrToInt(LExpr[eG1].Match[1], MTime);
end;

function IsGoCmd(const AStr: string; out WTime, BTime: integer): boolean;
// go wtime 600000 btime 600000
begin
  WTime := 0;
  BTime := 0;
  
  result :=
    LExpr[eG2].Exec(AStr)
    and LExpr[eWT].Exec(AStr)
    and LExpr[eBT].Exec(AStr)
    and TryStrToInt(LExpr[eWT].Match[1], WTime)
    and TryStrToInt(LExpr[eBT].Match[1], BTime);
end;

function IsGoCmd(const AStr: string; out WTime, BTime, MTG: integer): boolean;
// go wtime 59559 btime 56064 movestogo 38
begin
  WTime := 0;
  BTime := 0;
  MTG := 0;
  
  result :=
    LExpr[eG3].Exec(AStr)
    and LExpr[eWT].Exec(AStr)
    and LExpr[eBT].Exec(AStr)
    and TryStrToInt(LExpr[eG3].Match[1], MTG)
    and TryStrToInt(LExpr[eWT].Match[1], WTime)
    and TryStrToInt(LExpr[eBT].Match[1], BTime);
end;

function IsGoCmd(const AStr: string; out WTime, BTime, WInc, BInc: integer): boolean;
// go wtime 60000 btime 60000 winc 1000 binc 1000
begin
  WTime := 0;
  BTime := 0;
  WInc := 0;
  BInc := 0;
  
  result :=
    LExpr[eG4].Exec(AStr)
    and LExpr[eWT].Exec(AStr)
    and LExpr[eBT].Exec(AStr)
    and LExpr[eWI].Exec(AStr)
    and LExpr[eBI].Exec(AStr)
    and TryStrToInt(LExpr[eWT].Match[1], WTime)
    and TryStrToInt(LExpr[eBT].Match[1], BTime)
    and TryStrToInt(LExpr[eWI].Match[1], WInc)
    and TryStrToInt(LExpr[eBI].Match[1], BInc);
end;

function IsPerftCmd(const AStr: string; out ADepth: integer): boolean;
// perft 1
var
  i: integer;
begin
  ADepth := 0;
  result := WordPresent('perft', AStr) and TryStrToInt(GetWord(2, AStr), i);
  if result then
    ADepth := i;
end;

function IsSetOptionCmd(const AStr: string; out AName, AValue: string): boolean;
begin
  result := LExpr[eSO].Exec(AStr);
  if result then
  begin
    AName := LExpr[eSO].Match[1];
    AValue := LExpr[eSO].Match[2];
  end else
  begin
    AName := EmptyStr;
    AValue := EmptyStr;
  end;
end;

var
  e: TExpr;
  
initialization
  for e in TExpr do
    LExpr[e] := TRegExpr.Create(CExpr[e]);
  
finalization
  for e in TExpr do
    LExpr[e].Free;
  
end.
