
unit ChessTypes;

interface

type
  TChessPieceKindEx = (cpkPawn, cpkKnight, cpkBishop, cpkRook, cpkQueen, cpkKing, cpkNil);
  TChessPieceKind = cpkPawn..cpkKing;
  TChessPieceColorEx = (cpcWhite, cpcBlack, cpcNil);
  TChessPieceColor = cpcWhite..cpcBlack;

const
  CPieceSymbol: array[TChessPieceColor, TChessPieceKind] of char = (
    ('P', 'N', 'B', 'R', 'Q', 'K'),
    ('p', 'n', 'b', 'r', 'q', 'k')
  );

type
  TChessPiece = record
    kind: TChessPieceKindEx;
    color: TChessPieceColorEx;
  end;
  TBoard = array[1..8, 1..8] of TChessPiece;
  
  TCastlingData = record
    xk, xrWH, xrWA, xrBH, xrBA: integer; 
  end;
  
  TChessPositionData = record
    board: TBoard;
    activeColor: TChessPieceColor;
    castling: TCastlingData;
    enPassant: string;
    halfMoves: integer;
    fullMove: integer;
  end;
  TChessState = (csProgress, csCheckmate, csStalemate, csDraw);
  
function OtherColor(const aColor: TChessPieceColorEx): TChessPieceColorEx;
function ValidPromotionValue(const aValue: TChessPieceKindEx): TChessPieceKind;
function BoardToText(const AData: TChessPositionData): string;
(*
type
  TCastlingType = (ctWhiteH, ctWhiteA, ctBlackH, ctBlackA, ctNil);
*)
implementation

function OtherColor(const aColor: TChessPieceColorEx): TChessPieceColorEx;
begin
  if aColor = cpcNil then
    result := aColor
  else
    result := TChessPieceColor(1 - Ord(aColor));
end;

function ValidPromotionValue(const aValue: TChessPieceKindEx): TChessPieceKind;
begin
  if aValue in [cpkKnight, cpkBishop, cpkRook, cpkQueen] then
    result := TChessPieceKind(aValue)
  else
    result := cpkQueen;
end;

function BoardToText(const AData: TChessPositionData): string;
var
  x, y: integer;
begin
  result :=  '+   a b c d e f g h '#13#10#10;
  for y := 8 downto 1 do
  begin
    result := result + Chr(y + Ord('0')) + '   ';
    for x := 1 to 8 do
    begin
      if AData.board[x, y].color = cpcNil then
        if (x + y) mod 2 = 1 then
          result := result + '. '
        else
          result := result + ': '
      else
        result := result + CPieceSymbol[AData.board[x, y].color, AData.board[x, y].kind] + ' ';
    end;
    if y > 1 then result := result + #13#10;
  end;
end;

end.
