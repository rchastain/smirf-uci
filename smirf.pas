
unit Smirf;

interface

uses
  SysUtils, Classes;

type
  TSmirf = class
    type
      TFunction = function(MainCmd, SubCmd: ansichar; nrIn: integer; pIn: pansichar): pansichar; cdecl;
    public
      constructor Create;
      destructor Destroy; override;
      function Load(const ADir: string = ''): boolean;
      procedure Unload;
      function DoCmd(const MainCmd, SubCmd: ansichar; const nrIn: integer = 0; const pIn: string = ''): string;
    private
      FHandle: THandle;
      FFunction: TFunction;
  end;

implementation

uses
  Log;

constructor TSmirf.Create;
begin
  inherited;
  FHandle := 0;
  FFunction := nil;
end;

destructor TSmirf.Destroy;
begin
  Unload;
  inherited;
end;

function TSmirf.Load(const ADir: string): boolean;
const
  CLibraryName = 'SmirfEngine.dll';
var
  LLibraryName: string;
begin
  result := FALSE;
  
  if DirectoryExists(ADir) then
    LLibraryName := Concat(IncludeTrailingPathDelimiter(ADir), CLibraryName)
  else
    LLibraryName := CLibraryName;
  
  LogLn(Concat('Library name: ', LLibraryName), 1);
  
  if FileExists(LLibraryName) then
  begin
{$IFDEF MSWINDOWS}
    FHandle := LoadLibrary(LLibraryName);
{$ELSE}
    FHandle := 0;
{$ENDIF}
    if FHandle <> 0 then
    begin
      FFunction := TFunction(GetProcedureAddress(FHandle, '_DoCmd'));
      if FFunction = nil then
        FFunction := TFunction(GetProcedureAddress(FHandle, 'DoCmd'));
      if FFunction <> nil then
        result := TRUE
      else
        WriteLn(ErrOutput, 'Cannot get procedure address');
    end else
      WriteLn(ErrOutput, 'Cannot load library');
  end else
    WriteLn(ErrOutput, 'Cannot find ' + LLibraryName + ' [CurrentDir = ' + GetCurrentDir + ']');
end;

procedure TSmirf.Unload;
begin
  if FHandle <> 0 then
  begin
    FreeLibrary(FHandle);
  end;
end;

function TSmirf.DoCmd(const MainCmd, SubCmd: ansichar; const nrIn: integer = 0; const pIn: string = ''): string;
var
  p, res: pansichar;
  arr: array[0..255] of ansichar;
begin
  if pIn = '' then
    p := nil
  else
    p := StrPCopy(arr, pIn);
  res := FFunction(MainCmd, SubCmd, nrIn, p);
  if res = nil then
    result := ''
  else
    result := StrPas(res);
end;

end.
