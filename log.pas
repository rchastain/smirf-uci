
unit Log;

interface

uses
  SysUtils;

procedure OpenLog;
procedure CloseLog;
procedure LogLn(const ALine: string; const ALevel: integer; const AFlush: boolean = TRUE);
procedure SetLogLevel(const ALevel: integer);

implementation

var
  LFile: text;
  LLevel: integer;

procedure OpenLog;
var
  LName: string;
begin
  if LLevel > 0 then
  begin
    LName := ChangeFileExt(ParamStr(0), '.log');
    Assign(LFile, LName);
    if FileExists(LName) then
      Append(LFile)
    else
      Rewrite(LFile);
  end;
end;

procedure CloseLog;
begin
  if LLevel > 0 then
  begin
    Close(LFile);
  end;
end;

procedure LogLn(const ALine: string; const ALevel: integer; const AFlush: boolean);
begin
  if ALevel <= LLevel then
  begin
    WriteLn(LFile, Concat(DateTimeToStr(Now()), ' ', ALine));
    if AFlush then
      Flush(LFile);
  end;
end;

procedure SetLogLevel(const ALevel: integer);
begin
  LLevel := ALevel;
end;

initialization
  LLevel := 0;
end.
