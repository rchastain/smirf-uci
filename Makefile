
PC=fpc
DEBUG=false

ifeq ($(DEBUG),true)
	PFLAGS=-B -Mobjfpc -FUunits -Sah -dDEBUG -ghl
else
	PFLAGS=-B -Mobjfpc -FUunits -Sh -dRELEASE -CX -XX -Xs
endif

SRC=chesstypes.pas log.pas smirf.pas utils.pas xfen.pas

all: smirf

smirf: main.pas $(SRC)
	$(PC) $< $(PFLAGS) -osmirf.exe

%: %.pas $(SRC)
	$(PC) $< $(PFLAGS)

clean:
	del /q units\*.o
	del /q units\*.ppu
	del /q /s smirf.log

copy:
	copy /y smirf.exe BC-173g-X
	copy /y smirf.exe BC-176f
	copy /y smirf.exe BC-178n
	copy /y smirf.exe BC-180e

tag:
	git tag -a 0.0.2 -m "Smirf UCI 0.0.2"
	git push --tags origin
