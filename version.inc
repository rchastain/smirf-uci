
const
  CApp = 'Smirf UCI';
  CVer = '0.0.3';
  CAut = 'Reinhard Scharnagl, Roland Chastain';
  CBuildInfo = {$I %DATE%} + ' ' + {$I %TIME%} + ' Free Pascal ' + {$I %FPCVERSION%} + ' ' + {$I %FPCTARGETOS%};
