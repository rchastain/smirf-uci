
unit XFen;

interface

uses
  ChessTypes;

const
  FENSTARTPOSITION = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';

function EncodeChessPositionData(const aFENRecord: string = FENSTARTPOSITION): TChessPositionData;
function DecodeChessPositionData(const aPositionData: TChessPositionData): string;
function IsCastling(const AData: TChessPositionData; const AMove: string; out AComment: string): string;

implementation

uses
  SysUtils, Classes, RegExpr;

function XToChar(const x: integer; const a: char): char;
begin
  result := Chr(x + Ord(a) - 1);
end;

const
  CColorSymbol: array[TChessPieceColor] of char = ('w', 'b');
  TABLE = 'PNBRQK';

function EncodeChessPositionData(const aFENRecord: string = FENSTARTPOSITION): TChessPositionData;
var
  x, y, i: integer;
  c: char;
  e: TRegExpr;
begin
  e := TRegExpr.Create;
  with TStringList.Create do
  begin
    DelimitedText := aFENRecord;
    Assert(Count = 6);
    with result do
    begin
      x := 1;
      y := 8;
      i := 1;
      while i <= Length(Strings[0]) do
      begin
        c := Strings[0][i];
        case c of
          '/':
            begin
              x := 1;
              Dec(y);
            end;
          '1'..'8':
            while c > '0' do
            begin
              board[x, y].color := cpcNil;
              board[x, y].kind := cpkNil;
              Inc(x);
              Dec(c);
            end;
        else
          begin
            board[x, y].color := TChessPieceColor(Ord(c in ['a'..'z']));
            board[x, y].kind := TChessPieceKind(Pred(Pos(UpCase(c), TABLE)));
            Inc(x);
          end;
        end;
        Inc(i);
      end;
      activeColor := TChessPieceColor(Ord(Strings[1] = CColorSymbol[cpcBlack]));
      FillByte(castling, SizeOf(castling), 0);
      if Strings[2] <> '-' then
      begin
        e.Expression := '[ABCDEFGHKQ]{1,2}'; if e.Exec(Strings[2]) then begin
          x := 1; while (x <= 8) and (board[x, 1].kind <> cpkKing) do Inc(x); if x <= 8 then castling.xk := x; Assert(castling.xk <> 0);
          e.Expression := '[' + Copy('ABCDEFGH', Succ(castling.xk), 8 - castling.xk) + 'K]'; if e.Exec(Strings[2]) then begin
            c := e.Match[0][1]; x := 8; while (castling.xrWH = 0) and (x > castling.xk) do if (board[x, 1].kind = cpkRook) and ((c = 'K') or (c = XToChar(x, 'A'))) then castling.xrWH := x else Dec(x); Assert(castling.xrWH <> 0);
          end;
          e.Expression := '[' + Copy('ABCDEFGH', 1, Pred(castling.xk)) + 'Q]'; if e.Exec(Strings[2]) then begin
            c := e.Match[0][1]; x := 1; while (castling.xrWA = 0) and (x < castling.xk) do if (board[x, 1].kind = cpkRook) and ((c = 'Q') or (c = XToChar(x, 'A'))) then castling.xrWA := x else Inc(x); Assert(castling.xrWA <> 0);
          end;
        end;
        e.Expression := '[abcdefghkq]{1,2}'; if e.Exec(Strings[2]) then begin
          x := 1; while (x <= 8) and (board[x, 8].kind <> cpkKing) do Inc(x); if x <= 8 then castling.xk := x; Assert(castling.xk <> 0);
          e.Expression := '[' + Copy('abcdefgh', Succ(castling.xk), 8 - castling.xk) + 'k]'; if e.Exec(Strings[2]) then begin
            c := e.Match[0][1]; x := 8; while (castling.xrBH = 0) and (x > castling.xk) do if (board[x, 8].kind = cpkRook) and ((c = 'k') or (c = XToChar(x, 'a'))) then castling.xrBH := x else Dec(x); Assert(castling.xrBH <> 0);
          end;
          e.Expression := '[' + Copy('abcdefgh', 1, Pred(castling.xk)) + 'q]'; if e.Exec(Strings[2]) then begin
            c := e.Match[0][1]; x := 1; while (castling.xrBA = 0) and (x < castling.xk) do if (board[x, 8].kind = cpkRook) and ((c = 'q') or (c = XToChar(x, 'a'))) then castling.xrBA := x else Inc(x); Assert(castling.xrBA <> 0);
          end;
        end;
      end;
      enPassant := Strings[3];
      halfMoves := StrToInt(Strings[4]);
      fullMove := StrToInt(Strings[5]);
    end;
    Free;
  end;
  e.Free;
end;

function Interference(const aPositionData: TChessPositionData; const AXRook, AYRook, ADirection: integer): boolean;
var
  x: integer;
begin
  Assert(aPositionData.board[AXRook, AYRook].kind = cpkRook);
  result := FALSE;
  x := AXRook + ADirection;
  while (x >= 1) and (x <= 8) do
    if (aPositionData.board[x, AYRook].kind = cpkRook)
    and (aPositionData.board[x, AYRook].color = aPositionData.board[AXRook, AYRook].color) then
      Exit(TRUE)
    else
      Inc(x, ADirection);
end;

function DecodeChessPositionData(const aPositionData: TChessPositionData): string;
var
  x, y, n: integer;
  LCastling: string;
begin
  with aPositionData do
  begin
    result := '';
    x := 1;
    y := 8;
    while y >= 1 do
    begin
      if board[x, y].kind = cpkNil then
      begin
        n := 0;
        while (x + n <= 8) and (board[x + n, y].kind = cpkNil) do
          Inc(n);
        result := Concat(result, IntToStr(n));
        Inc(x, n);
      end else
      begin
        result := Concat(result, CPieceSymbol[board[x, y].color, board[x, y].kind]);
        Inc(x);
      end;
      if x > 8 then
      begin
        if y > 1 then
          result := Concat(result, '/');
        x := 1;
        Dec(y);
      end;
    end;
    
    LCastling := '';
    if castling.xrWH <> 0 then if Interference(aPositionData, castling.xrWH, 1,  1) then LCastling := LCastling + XToChar(castling.xrWH, 'A') else LCastling := LCastling + 'K';
    if castling.xrWA <> 0 then if Interference(aPositionData, castling.xrWA, 1, -1) then LCastling := LCastling + XToChar(castling.xrWA, 'A') else LCastling := LCastling + 'Q';
    if castling.xrBH <> 0 then if Interference(aPositionData, castling.xrBH, 8,  1) then LCastling := LCastling + XToChar(castling.xrBH, 'a') else LCastling := LCastling + 'k';
    if castling.xrBA <> 0 then if Interference(aPositionData, castling.xrBA, 8, -1) then LCastling := LCastling + XToChar(castling.xrBA, 'a') else LCastling := LCastling + 'q';
    if LCastling = '' then
      LCastling := '-';
    
    result := Format(
      '%s %s %s %s %d %d',
      [
        result,
        CColorSymbol[activeColor],
        LCastling,
        enPassant,
        halfMoves,
        fullMove
      ]
    );
  end;
end;

function SquareToStr(x, y: integer): string;
begin
  result := Chr(x + Ord('a') - 1) + Chr(y + Ord('1') - 1);
end;

function IsCastling(const AData: TChessPositionData; const AMove: string; out AComment: string): string;
var
  x, y, xx, yy, xxx: integer;
begin
  result := '';
  AComment := '';
  if Length(AMove) = 4 then
  begin
    x  := Ord(AMove[1]) - Ord('a') + 1;
    y  := Ord(AMove[2]) - Ord('1') + 1;
    xx := Ord(AMove[3]) - Ord('a') + 1;
    yy := Ord(AMove[4]) - Ord('1') + 1;
    xxx := 0;
    if (x >= 1) and (x <= 8)
    and (y >= 1) and (y <= 8)
    and (xx >= 1) and (xx <= 8)
    and (yy >= 1) and (yy <= 8) then
      with AData do
      begin
        if (board[x, y].color = board[xx, yy].color)
        and (board[x, y].kind = cpkKing)
        and (board[xx, yy].kind = cpkRook) then
          if (xx > x) then
          begin
            if (y = 1) and (xx = castling.xrWH) then
            begin
              AComment := 'K';
              xxx := 7;
            end else
            if (y = 8) and (xx = castling.xrBH) then
            begin
              AComment := 'k';
              xxx := 7;
            end;
          end else
          begin
            if (y = 1) and (xx = castling.xrWA) then
            begin
              AComment := 'Q';
              xxx := 3;
            end else
            if (y = 8) and (xx = castling.xrBA) then
            begin
              AComment := 'q';
              xxx := 3;
            end;
          end;
          
        if AComment <> '' then
          if Abs(xxx - x) < 2 then
          begin
            AComment := AComment + ' ok';
            result := AMove;
          end else
          begin
            result := SquareToStr(x, y) + SquareToStr(xxx, yy);
            AComment := AComment + ' ' + AMove + ' -> ' + result;
          end;
          
      end;
  end;
end;

end.
