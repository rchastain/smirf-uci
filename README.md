# SMIRF-UCI

UCI interface for SMIRF chess engine.

Inspired by [Smirfoglot](https://home.hccnet.nl/h.g.muller/smirf.html).

Supports FRC.

## Compilation

SMIRF-UCI is compiled with Free Pascal.

Provided you have *make* and Free Pascal installed, you can compile SMIRF by opening a console and typing `make`.

## Usage

The repository includes scripts for playing against SMIRF in WinBoard or in XBoard. Indeed, SMIRF-UCI works also under Linux, with *wine*.

Below, a screenshot of SMIRF-UCI loaded in XBoard:

![Screenshot](screenshots/xboard-chess960.png)

SMIRF-UCI supports traditional chess and chess 960. If you want SMIRF to play one of the other variants that he is able to play, use Smirfoglot.

## Command-line option

In version 0.0.3, a command-line option has been added, allowing to choose a log level, from 0 (no log) to 3 (very verbose):

```
smirf.exe --loglevel=1
```

The default value is 0.
