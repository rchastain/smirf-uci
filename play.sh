#!/bin/sh

rm -f *.log

SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`
echo $SCRIPTPATH

xboard \
-size small \
-debug -debugfile $SCRIPTPATH/xboard.debug \
-fcp "wine smirf.exe --loglevel=3" -fd $SCRIPTPATH -fUCI \
-tc 10 -inc 1
