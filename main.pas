
{$ASSERTIONS ON}

uses
  SysUtils, Classes, Smirf, Utils, Log, ChessTypes, XFen, RegExpr;

{$I version.inc}
  
procedure SendToUser(const AText: string; const AFlush: boolean = TRUE);
begin
  WriteLn(output, AText);
  if AFlush then
    Flush(output);
end;

type
  TListener = class(TThread)
    protected
      procedure Execute; override;
  end;

var
  LSmirf: TSmirf;
  
procedure TListener.Execute;
const
  CDelay = 30;
begin
  while LSmirf.DoCmd('C', 'B') <> '' do // Engine busy
    Sleep(CDelay);
  
  SendToUser(Format('bestmove %s', [LSmirf.DoCmd('C', 'A')])); // Best move
end;

function MoveList(): string;
var
  LRes: string;
  i: integer;
begin
  i := 0;
  LRes := LSmirf.DoCmd('M', 'A', i); // Legal move number (i + 1)
  while LRes <> '' do
  begin
    if i = 0 then
      result := LRes
    else
      result := result + ' ' + LRes;
    Inc(i);
    LRes := LSmirf.DoCmd('M', 'A', i);
  end;
end;

function CountMoves(
  const ADepth: integer;
  const AFirstCall: boolean = TRUE
): integer;
var
  i: integer;
  r: string;
begin
  if ADepth = 0 then
    Exit(1);
  result := 0;
  i := 0;
  r := LSmirf.DoCmd('M', 'A', i);
  while r <> '' do
  begin
    if (ADepth > 1) then
    begin
      Assert(LSmirf.DoCmd('E', 'M', 0, r) <> ''); // Do move
      Inc(result, CountMoves(Pred(ADepth), FALSE));
      Assert(LSmirf.DoCmd('E', '-') <> ''); // Undo move
    end else
      Inc(result);
    if AFirstCall then
      WriteLn(r);
    Inc(i);
    r := LSmirf.DoCmd('M', 'A', i);
  end;
end;

const
  CUser = 'Donationware Version - donate !';
  CUserOk = CUser;
  CKey = '342f6f5e 026b3179';
  CKeyOk = 'Smirf';
  
var
  LCmd, LAux: string;
  LIdx: integer;
  LMov: string;
  LMTime, LWTime, LBTime, LMTG, LWInc, LBInc: integer;
  LData: TChessPositionData;
  LComment: string;
  LDepth: integer;
  LOptionName, LOptionValue: string;
  LLogLevel: integer;
  LLogLevelExpr: TRegExpr;
  i: integer;
  
begin
  if ParamCount > 0 then
  begin
    LLogLevelExpr := TRegExpr.Create('--loglevel=(\d)');
    for i := 1 to ParamCount do
      if LLogLevelExpr.Exec(ParamStr(i)) then
      begin
        LLogLevel := StrToIntDef(LLogLevelExpr.Match[1], 0);
        Assert(LLogLevel in [0..3]);
        SetLogLevel(LLogLevel);
      end;
    LLogLevelExpr.Free;
  end;
  
  OpenLog;
  
  LSmirf := TSmirf.Create;
  if not LSmirf.Load(ExtractFileDir(ParamStr(0))) then
  begin
    LSmirf.Free;
    CloseLog;
    Halt;
  end;
  
  LogLn(Format('%s %s %s', [CApp, CVer, CBuildInfo]), 1);
  
  Assert(LSmirf.DoCmd('L', 'N', 0, CUser) = CUserOk); // User name
  Assert(LSmirf.DoCmd('L', 'K', 0, CKey) = CKeyOk); // User key
  
  while not EOF do
  begin
    ReadLn(input, LCmd);
    LogLn(Concat('<- ', LCmd), 1);
    if LCmd = 'quit' then
      Break
    else
      if LCmd = 'uci' then
      begin
        SendToUser(Format('id name %s %s', [CApp, CVer]), FALSE);
        SendToUser(Format('id author %s', [CAut]), FALSE);
        SendToUser(Format('info realname %s realversion %s realauthor %s', [
          LSmirf.DoCmd('I', 'E'), // Engine name
          LSmirf.DoCmd('I', 'V'), // Engine version
          LSmirf.DoCmd('I', 'A')  // Engine author
        ]));
        SendToUser('option name UCI_Chess960 type check default true', FALSE);
        SendToUser('uciok');
        
        Assert(LSmirf.DoCmd('C', 'H', 32) <> ''); // Set hash size
        LogLn(Concat('Hash size: ', LSmirf.DoCmd('C', 'H', -1)), 1); // Get hash size
      end else
        if LCmd = 'isready' then
          SendToUser('readyok')
        else
          if LCmd = 'ucinewgame' then
          else
            if BeginsWith('position ', LCmd) then
            begin
              if WordPresent('startpos', LCmd) then
                LAux := FENSTARTPOSITION
              else if WordPresent('fen', LCmd) then
              begin
                LAux := GetFen(LCmd);
                LData := EncodeChessPositionData(LAux);
                LAux := DecodeChessPositionData(LData);
              end else
                Assert(FALSE);
              
              Assert(LSmirf.DoCmd('E', 'F', 0, LAux) <> ''); // Enter position
              if WordPresent('moves', LCmd) then
                for LIdx := 4 to WordsNumber(LCmd) do
                begin
                  LMov := GetWord(LIdx, LCmd);
                  if IsChessMove(LMov) then
                  begin
                    if (Length(LMov) > 4) and (LMov[5] in ['r', 'n', 'b', 'q']) then
                    begin
                      LAux := LMov;
                      LAux[5] := UpCase(LAux[5]);
                      LogLn(Format('Promotion: %s -> %s', [LMov, LAux]), 2);
                      LMov := LAux;
                    end;
                    LData := EncodeChessPositionData(
                      LSmirf.DoCmd('F', 'S') // Get current position
                    );
                    LAux := IsCastling(LData, LMov, LComment);
                    if LAux <> '' then
                    begin
                      LogLn(Concat('Castling: ', LComment), 2);
                      LMov := LAux;
                    end;
                    if LSmirf.DoCmd('E', 'M', 0, LMov) = '' then // Do move
                    begin
                      LogLn(Format('Could not do move: %s', [LMov]), 1);
                      LogLn(LSmirf.DoCmd('F', 'S'), 1);
                      LogLn(MoveList(), 1);
                      Assert(FALSE);
                    end;
                  end else
                    LogLn(Format('Not a chess move: %s', [LMov]), 3);
                end;
            end else
              if BeginsWith('go', LCmd) then
              begin
                if IsGoCmd(LCmd, LWTime, LBTime, LWInc, LBinc) then // go wtime 60000 btime 60000 winc 1000 binc 1000
                begin
                  LogLn(Format('Command parameters: wtime %d btime %d winc %d binc %d', [LWTime, LBTime, LWInc, LBinc]), 3);
                  Assert(LSmirf.DoCmd('T', 'W', LWInc + LWTime) <> '');
                  Assert(LSmirf.DoCmd('T', 'B', LBinc + LBTime) <> '');
                  Assert(LSmirf.DoCmd('T', '#', 0) <> ''); // Time is for all moves
                end else
                if IsGoCmd(LCmd, LWTime, LBTime, LMTG) then // go wtime 59559 btime 56064 movestogo 38
                begin
                  LogLn(Format('Command parameters: wtime %d btime %d movestogo %d', [LWTime, LBTime, LMTG]), 3);
                  Assert(LSmirf.DoCmd('T', 'W', LWTime div LMTG) <> '');
                  Assert(LSmirf.DoCmd('T', 'B', LBTime div LMTG) <> '');
                  Assert(LSmirf.DoCmd('T', '#', 1) <> ''); // Time is for one move
                end else
                if IsGoCmd(LCmd, LWTime, LBTime) then // go wtime 600000 btime 600000
                begin
                  LogLn(Format('Command parameters: wtime %d btime %d', [LWTime, LBTime]), 3);
                  Assert(LSmirf.DoCmd('T', 'W', LWTime) <> '');
                  Assert(LSmirf.DoCmd('T', 'B', LBTime) <> '');
                  Assert(LSmirf.DoCmd('T', '#', 0) <> ''); // Time is for all moves
                end else
                if IsGoCmd(LCmd, LMTime) then // go movetime 500
                begin
                  LogLn(Format('Command parameter: movetime %d', [LMTime]), 3);
                  Assert(LSmirf.DoCmd('T', 'W', LMTime) <> '');
                  Assert(LSmirf.DoCmd('T', 'B', LMTime) <> '');
                  Assert(LSmirf.DoCmd('T', '#', 1) <> ''); // Time is for one move
                end else
                  Assert(FALSE, 'Cannot parse go command');
                
                Assert(LSmirf.DoCmd('C', '+') <> ''); // Start thinking
                
                with TListener.Create(TRUE) do
                begin
                  FreeOnTerminate := TRUE;
                  Priority := tpHigher;
                  Start;
                end;
              end else
                if IsPerftCmd(LCmd, LDepth) then
                  SendToUser(Format('perft(%d) = %d', [
                    LDepth, CountMoves(LDepth)
                  ]))
                else
                  if IsSetOptionCmd(LCmd, LOptionName, LOptionValue) then
                    LogLn(Format('Command parameters: name %s value %s', [LOptionName, LOptionValue]), 3);
  end;
  
  LSmirf.Free;
  
  CloseLog;
end.
